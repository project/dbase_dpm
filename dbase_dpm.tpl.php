<?php
/**
 * @file
 * Provide the HTML output for krumo output.
 */
?>

<h2><?php print $comment; ?></h2>
<span class="dbase-dpm-subtitle">
  Called from <code><?php print $file; ?></code> line <code><?php print $line; ?></code>
</span>
  <?php print $krumo; ?>
  Unserialized value to copy:
<div class="dbase-dpm-unserialized"><?php print $unserialized; ?></div>
